= How to make casual contributions
Fedora Documentation Team <https://discussion.fedoraproject.org/tag/docs>
:revdate: 2023-26-06
:page-pagination:

[abstract]
To make changes in Docs page hosted on GitLab, you have a built-in web editor that makes your first contribution easier than ever. Start editing documentation on GitLab Web IDE that you can launch in one click. You do not need special permissions or write access to the original project.

- There is no need to install apps or clone repos on your local computer, or run Git commands in terminal.

- There is no risk of breaking anything.

- This guide is for GitLab-hosted repo.

== How to edit document with Web IDE

=== Step 1. Select a page to edit

- Find a Docs page you want to edit.
- Navigate to the top right and locate three buttons.
- Click the middle *edit the page* button to make corrections.

NOTE: The left button forwards to the version history of the opened Docs page as a reference. The right button *report an issue* is when you report any change or update required.

|===
a|image::1CUT-changeIdentified.png[align="center",role=black-border]
|===

|===
a|image::1A-changeIdentified.png[align="center",width=940,role=black-border]
|===

- Log in to your existing GitLab account
- Use the https://gitlab.com/groups/fedora/-/saml/sso[SAML link] to link your account to FAS
- You will be redirected and asked for your FAS credentials if needed
- Grant permission to Fedora

Create an account if you do not have one yet (see the image below). 

|===
a|image::2-GitLabSignin.png[align="center",width=940,role=black-border]
|===

NOTE: This contributor guide walks through the new Web IDE, introduced in GitLab 15.7 release. The guide also reflects a recent change about *how to update fork*.

=== Step 2. Create your project (copy)

Click the *Open in Web IDE* button. Alternatively, press . (dot) in keyboard to launch Web IDE.

|===
a|image::3-EditPage.png[align="center",width=940,role=black-border]
|===

==== Fork a project first time

Click the *Fork* button.

The *Fork* button appears only the first time you contribute to the repository of a given Docs page. If you are contributing to the Fedora project for the first time and you do not have write access for the repository you want to contribute to, you need to fork a project.

A fork is a personal copy of the repository, which you create in your namespace.

|===
a|image::4-EditFork.png[align="center",width=940,role=black-border]
|===

==== If you have forked a project before

If you have a forked project before, you will be forwarded automatically to the next step without the *Fork* button.

If you do not launch Web IDE from your fork, *edit fork in Web IDE* button will appear in lieu of the *Open in Web IDE* button. Then you will be prompted to *go to fork* and checkout to your fork.

- Go to your forked project.
- Check the status of your fork underneath Web IDE button.

If your fork shows *Up to date with the upstream repository*, go to step 3 in Web IDE.

.Fork is up to date
image::gitlab-ui/update-fork-no.png[]

If your fork is behind upstream, you will see UI text how far your fork is behind.

.Update fork
image::gitlab-ui/update-fork-yes.png[]

- Click *Update fork* button.
- The fork will display *Up to date with the upstream repository*
- *Update fork* button disappears after fork is updated

=== Step 3. Create a new branch (version)

On your forked project, create a new branch and switch to it following the steps below.

- On the status bar underneath in the lower-left corner, click the current branch name (normally main).
- From the dropdown list, select Create new branch….
- Type the branch name that is specific to a task.
- Press Enter.
- Checkout to a new branch. Click Yes to the following pop-up box.

----
Are you sure you want to checkout <branch-name>? Any unsaved changes will be lost.
----

=== Step 4. Edit and save changes

Now, make the necessary changes in Web IDE. To write with consistency, refer to xref:contributing-docs/style-guide.adoc#prerequisites[The docs style guide]

.Editor UI
image::gitlab-ui/editor.png[]

Once you've finished, select the source control button and click the file underneath *changes* below *Commit & Push* button) to view your changes.

Type a commit message and click *Commit & Push*. Click No to commit to existing branch. 

=== Step 5. Create Merge Request (MR)

In the lower-right corner, you will find three options - *create MR*, *Go to project*, *Continue working*.

.Create MR button
image::gitlab-ui/commit.png[]

- Click *Create MR* to create merge request. 
- Edit the title and the description of the page.
- Scroll down to the blue *Create merge request* button and click it.

NOTE: *Go to project* option is suitable when you made small commits by stage and want to squash them into one. *Continue working* is when you want to make smaller commits by sub tasks. If you select *Create MR*, the UI will navigate to merge request.

.Merge Request
image::gitlab-ui/create-mr.png[]

If merge request is successful, you will see the following result in the lower-middle of merge request.

----
Ready to merge by members who can write to the target branch.
----

=== Step 6. Review

Your request will be reviewed by Docs team. You will expect one of the following actions.

- Reviewers ask you to revise your changes

- Approve

- Reject and close MR within reason

Whatever the case it is, MR comments facilitate collaboration between contributors and reviewers. Everyone can learn from those comments and corrections.

Once your MR is approved, the process is complete.

Thanks for your contribution!
